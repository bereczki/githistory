package com.thotsoft.githistory.server.model;

import com.thotsoft.githistory.api.model.Tag;

public class TagItem {
    private Tag tag;
    private String id;

    public Tag getTag() {
        return tag;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TagItem tagItem = (TagItem) o;

        if (!tag.equals(tagItem.tag)) {
            return false;
        }
        return id.equals(tagItem.id);
    }

    @Override
    public int hashCode() {
        int result = tag.hashCode();
        result = 31 * result + id.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "TagItem{" +
                "tag=" + tag +
                ", id='" + id + '\'' +
                '}';
    }
}
