package com.thotsoft.githistory.server.services;

import com.thotsoft.githistory.api.model.Commit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;

public class ElasticCommitItemIterator implements Iterator {
    private static final Logger logger = LoggerFactory.getLogger(ElasticCommitItemIterator.class);

    private final CommitRest gitRest;
    private final String scrollId;
    private List<Commit> list;
    private int current = 0;

    ElasticCommitItemIterator(CommitRest gitRest, List<Commit> list, String scrollId) {
        this.gitRest = gitRest;
        this.list = list;
        this.scrollId = scrollId;
    }

    @Override
    public boolean hasNext() {
        if (current < list.size()) {
            return true;
        } else {
            current = 0;
            return nextScroll();
        }
    }

    @Override
    public Commit next() {
        if (!hasNext()) {
            return null;
        }
        return list.get(current++);
    }

    private boolean nextScroll() {
        try (InputStream stream = gitRest.scrolling(20, scrollId)) {
            list = new GitUtility().readCommit(stream);
            return !list.isEmpty();
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
        return false;
    }
}
