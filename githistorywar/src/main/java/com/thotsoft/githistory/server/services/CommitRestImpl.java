package com.thotsoft.githistory.server.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.thotsoft.githistory.api.model.Commit;
import org.apache.http.Header;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpHost;
import org.apache.http.HttpStatus;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.message.BasicHeader;
import org.elasticsearch.client.RestClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public class CommitRestImpl implements CommitRest {
    private static final Logger logger = LoggerFactory.getLogger(CommitRestImpl.class);
    private final Header header = new BasicHeader(HttpHeaders.CONTENT_TYPE, "application/json; charset=UTF-8");
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss,SSS",
            Locale.ENGLISH);
    private final Gson gson = new GsonBuilder().setDateFormat(dateFormat.toPattern()).create();
    private final String endPoint;
    private final String indexName;
    private RestClient restClient = null;
    private final GitUtility utility;

    public CommitRestImpl(String indexName, String typeName) {
        this.indexName = indexName;
        this.endPoint = "/" + indexName + "/" + typeName + "/";
        restClient = RestClient.builder(new HttpHost(System.getProperty("host"),
                Integer.valueOf(System.getProperty("port")), System.getProperty("scheme"))).build();
        utility = new GitUtility();
    }

    @Override
    public boolean isIndexExists() throws IOException {
        try {
            return restClient.performRequest("HEAD", "/" + indexName, header).getStatusLine()
                    .getStatusCode() == HttpStatus.SC_OK;
        } catch (final IOException e) {
            logger.error(e.getMessage(), e);
            throw new IOException(e);
        }
    }

    @Override
    public void createIndex() throws IOException {
        try {
            restClient.performRequest("PUT", "/" + indexName, new HashMap<>(),
                    EntityBuilder.create().setBinary(getCommitPropertiesForCreate()
                            .getBytes(StandardCharsets.UTF_8)).build(), header);
        } catch (final IOException e) {
            logger.error(e.getMessage(), e);
            throw new IOException(e);
        }
    }

    @Override
    public void removeIndex() throws IOException {
        try {
            restClient.performRequest("DELETE", "/" + indexName, header);
        } catch (final IOException e) {
            logger.error(e.getMessage(), e);
            throw new IOException(e);
        }
    }

    @Override
    public Commit getCommit(String id) throws IOException {
        try (InputStream inputStream = restClient.performRequest("GET",
                endPoint + id, header).getEntity().getContent()) {
            return utility.readCommitJson(inputStream);
        } catch (final IOException e) {
            logger.error(e.getMessage(), e);
            throw new IOException(e);
        }
    }

    @Override
    public Iterator<Commit> getCommits(final int documentPerScroll, final StringBuilder generatedScrollId)
            throws IOException {
        if (documentPerScroll > 10000 || documentPerScroll < 1) {
            throw new IllegalArgumentException("Wrong size!");
        }
        try (InputStream stream = restClient.performRequest("GET",
                endPoint + "_search?scroll=20m", new HashMap<>(),
                EntityBuilder.create().setBinary(GitUtility.getSearchJSON(documentPerScroll)
                        .getBytes(StandardCharsets.UTF_8)).build(),
                header).getEntity().getContent()) {

            List<Commit> list = utility.readCommit(stream, generatedScrollId);
            return new ElasticCommitItemIterator(this, list, generatedScrollId.toString());
        } catch (final IOException e) {
            logger.error(e.getMessage(), e);
            throw new IOException(e);
        }
    }

    @Override
    public InputStream scrolling(int scroll, String scrollId) throws IOException {
        try {
            return restClient.performRequest("POST", "/_search/scroll", new HashMap<>(),
                    EntityBuilder.create().setText(getScrollJSON(scroll, scrollId)).build(), header)
                    .getEntity().getContent();
        } catch (final IOException e) {
            logger.error(e.getMessage(), e);
            throw new IOException(e);
        }
    }

    @Override
    public boolean addCommit(Commit commit) throws IOException {
        try {
            return restClient.performRequest("POST", endPoint, new HashMap<>(),
                    EntityBuilder.create().setBinary(writeJsonStream(commit).getBytes(StandardCharsets.UTF_8)).build(), header)
                    .getStatusLine().getStatusCode() == HttpStatus.SC_CREATED;
        } catch (final IOException e) {
            logger.error(e.getMessage(), e);
            throw new IOException(e);
        }
    }

    @Override
    public boolean removeCommit(String id) throws IOException {
        try {
            return restClient.performRequest("DELETE", endPoint + id, header).getStatusLine()
                    .getStatusCode() == HttpStatus.SC_OK;
        } catch (final IOException e) {
            logger.error(e.getMessage(), e);
            throw new IOException(e);
        }
    }

    @Override
    public void close() throws IOException {
        try {
            restClient.close();
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            throw new IOException(e);
        }
    }

    /**
     * @param commit SimpleLog entity
     * @return Json string of log entity
     */
    private String writeJsonStream(final Commit commit) {
        return gson.toJson(commit, Commit.class);
    }

    /**
     * POST /_search/scroll
     *
     * @param min Lifetime of scroll in minute
     * @param id  Id of scroll
     * @return Json string to get a scroll
     */
    private static String getScrollJSON(final int min, final String id) {
        return "{\n" +
                "    \"scroll\" : \"" + min + "m\", \n" +
                "    \"scroll_id\" : \"" + id + "\" \n" +
                "}";
    }

    /**
     * @return Json string for create SimpleLog index
     */
    private String getCommitPropertiesForCreate() {
        return "{" +
                "\"mappings\" : {\n" +
                "        \"" + endPoint.substring(indexName.length() + 1) + "\" : {\n" +
                "            \"properties\" : {\n" +
                "                \"id\" : { \"type\" : \"text\" },\n" +
                "                \"author\" : { \"type\" : \"text\" },\n" +
                "                \"message\" : { \"type\" : \"text\" },\n" +
                "                \"parents\" : { \"type\" : \"text\" },\n" +
                "                \"addedLines\" : { \"type\" : \"integer\" },\n" +
                "                \"removedLines\" : { \"type\" : \"integer\" },\n" +
                "                \"changedFiles\" : { \"type\" : \"integer\" },\n" +
                "                \"date\" : { \"type\" : \"date\"," +
                "                             \"format\": \"yyyy-MM-dd HH:mm:ss,SSS\" }\n" +
                "            }\n" +
                "        }\n" +
                "    }\n" +
                "}";
    }
}
