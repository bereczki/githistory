package com.thotsoft.githistory.server.services;

import com.thotsoft.githistory.api.model.Commit;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

public interface CommitRest {
    boolean isIndexExists() throws IOException;

    void createIndex() throws IOException;

    void removeIndex() throws IOException;

    Commit getCommit(String id) throws IOException;

    Iterator<Commit> getCommits(int documentPerScroll, StringBuilder generatedScrollId) throws IOException;

    InputStream scrolling(int scroll, String scrollId) throws IOException;

    boolean addCommit(Commit commit) throws IOException;

    boolean removeCommit(String id) throws IOException;

    void close() throws IOException;
}
