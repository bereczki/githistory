package com.thotsoft.githistory.server.model;

import com.thotsoft.githistory.api.model.Commit;

public class CommitItem {
    private Commit commit;
    private String id;

    public Commit getCommit() {
        return commit;
    }

    public void setCommit(Commit commit) {
        this.commit = commit;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CommitItem that = (CommitItem) o;

        if (!commit.equals(that.commit)) {
            return false;
        }
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        int result = commit.hashCode();
        result = 31 * result + id.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "CommitItem{" +
                "commit=" + commit +
                ", id='" + id + '\'' +
                '}';
    }
}
