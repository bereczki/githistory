package com.thotsoft.githistory.server;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.thotsoft.githistory.api.GitService;
import com.thotsoft.githistory.api.model.Commit;
import com.thotsoft.githistory.api.model.Different;
import com.thotsoft.githistory.api.model.Tag;
import com.thotsoft.githistory.server.services.CommitRest;
import com.thotsoft.githistory.server.services.CommitRestImpl;
import com.thotsoft.githistory.server.services.TagRest;
import com.thotsoft.githistory.server.services.TagRestImpl;
import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.LogCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.diff.DiffFormatter;
import org.eclipse.jgit.diff.Edit;
import org.eclipse.jgit.diff.RawTextComparator;
import org.eclipse.jgit.errors.IncorrectObjectTypeException;
import org.eclipse.jgit.errors.MissingObjectException;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.lib.RepositoryBuilder;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.transport.JschConfigSessionFactory;
import org.eclipse.jgit.transport.OpenSshConfig;
import org.eclipse.jgit.transport.SshSessionFactory;
import org.eclipse.jgit.transport.SshTransport;
import org.eclipse.jgit.util.FS;
import org.eclipse.jgit.util.io.DisabledOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Remote;
import javax.ejb.Stateful;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

@Stateful
@Remote(GitService.class)
public class GitServiceImpl implements GitService, AutoCloseable {
    private static final Logger logger = LoggerFactory.getLogger(GitServiceImpl.class);
    private final CommitRest commitRest = new CommitRestImpl("githistory", "commit");
    private final TagRest tagRest = new TagRestImpl("githistory", "tag");
    private Map<Commit, Different<Commit>> differentsCommits = new HashMap<>();
    private Map<String, Tag> tagMap = new HashMap<>();
    private Repository repository;
    private Git git;

    @Override
    public void gitRemote(String git, String SSHPrivateKey) throws IOException {
        SshSessionFactory sshSessionFactory = new JschConfigSessionFactory() {
            @Override
            protected void configure(OpenSshConfig.Host hc, Session session) {
            }

            @Override
            protected JSch createDefaultJSch(FS fs) throws JSchException {
                JSch defaultJSch = super.createDefaultJSch(fs);
                try {
                    File tmp = File.createTempFile("privateKey", "tmp");
                    PrintWriter writer = new PrintWriter(new FileWriter(tmp));
                    writer.print(SSHPrivateKey);
                    writer.flush();
                    writer.close();
                    defaultJSch.addIdentity(tmp.getAbsolutePath());
                } catch (IOException e) {
                    logger.error(e.getMessage(), e);
                }
                return defaultJSch;
            }
        };
        try {
            cloneRepository(git, sshSessionFactory);
        } catch (GitAPIException e) {
            logger.error(e.getMessage(), e);
            throw new IOException(e);
        }
    }

    @Override
    public void gitRemote(String git, String SSHPrivateKey, String password) throws IOException {
        SshSessionFactory sshSessionFactory = new JschConfigSessionFactory() {
            @Override
            protected void configure(OpenSshConfig.Host hc, Session session) {
            }

            @Override
            protected JSch createDefaultJSch(FS fs) throws JSchException {
                JSch defaultJSch = super.createDefaultJSch(fs);
                try {
                    File tmp = File.createTempFile("privateKey", "tmp");
                    PrintWriter writer = new PrintWriter(new FileWriter(tmp));
                    writer.print(SSHPrivateKey);
                    writer.flush();
                    writer.close();
                    defaultJSch.addIdentity(tmp.getAbsolutePath(), password);
                } catch (IOException e) {
                    logger.error(e.getMessage(), e);
                }
                return defaultJSch;
            }
        };
        try {
            cloneRepository(git, sshSessionFactory);
        } catch (GitAPIException e) {
            logger.error(e.getMessage(), e);
            throw new IOException(e);
        }
    }

    @Override
    public void gitLocal(String pathToGit) throws IOException {
        try {
            repository = new RepositoryBuilder().setGitDir(new File(pathToGit)).build();
            git = new Git(repository);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            throw new RuntimeException("Something is wrong with the git!", e);
        }
    }

    @Override
    public List<String> listCommits() throws IOException {
        if (repository == null) {
            logger.error("Repo was null!");
            throw new NullPointerException("Repository was null!");
        }
        synchronized (commitRest) {
            if (!commitRest.isIndexExists()) {
                commitRest.createIndex();
            }
        }
        List<String> commitList = new ArrayList<>();
        try {
            differentsCommits.clear();
            for (RevCommit commit : git.log().all().call()) {
                commitList.add(commit.getId().getName());
                if (commit.getParents().length > 0) {
                    differentsCommits.put(toCommit(commit), countDifferences(commit, commit.getParent(0)));
                } else {
                    Different<Commit> different = new Different<>();
                    different.setFirst(toCommit(commit));
                    differentsCommits.put(toCommit(commit), different);
                }
            }
        } catch (GitAPIException | IOException e) {
            logger.error(e.getMessage(), e);
            throw new IOException(e);
        }
        return commitList;
    }

    @Override
    public List<String> listCommits(Date fromDate, Date toDate) throws IOException {
        if (repository == null) {
            logger.error("Repo was null!");
            throw new NullPointerException("Repository was null!");
        }
        synchronized (commitRest) {
            if (!commitRest.isIndexExists()) {
                commitRest.createIndex();
            }
        }
        List<String> commitList = new ArrayList<>();
        try {
            differentsCommits.clear();
            for (RevCommit commit : git.log().all().call()) {
                Date commitDate = commit.getCommitterIdent().getWhen();
                if (commitDate.after(fromDate) && commitDate.before(toDate)) {
                    commitList.add(commit.getId().getName());
                }
                if (commit.getParents().length > 0) {
                    differentsCommits.put(toCommit(commit), countDifferences(commit, commit.getParent(0)));
                } else {
                    Different<Commit> different = new Different<>();
                    different.setFirst(toCommit(commit));
                    differentsCommits.put(toCommit(commit), different);
                }
            }
        } catch (GitAPIException | IOException e) {
            logger.error(e.getMessage(), e);
            throw new IOException(e);
        }
        return commitList;
    }

    @Override
    public List<String> listTags() throws IOException {
        if (repository == null) {
            logger.error("Repo was null!");
            throw new NullPointerException("Repository was null!");
        }
        synchronized (tagRest) {
            if (!tagRest.isIndexExists()) {
                tagRest.createIndex();
            }
        }
        List<String> tagList = new ArrayList<>();
        try {
            List<Ref> call = git.tagList().call();
            for (Ref ref : call) {
                LogCommand log = git.log();
                Ref peeledRef = repository.peel(ref);
                if (peeledRef.getPeeledObjectId() != null) {
                    log.add(peeledRef.getPeeledObjectId());
                    tagList.add(peeledRef.getPeeledObjectId().getName());
                } else {
                    log.add(ref.getObjectId());
                    tagList.add(ref.getObjectId().getName());
                }

                Tag elasticTag = calculateDifferents(ref, log);
                tagMap.put(elasticTag.getId(), elasticTag);
            }
        } catch (GitAPIException | MissingObjectException | IncorrectObjectTypeException e) {
            logger.error(e.getMessage(), e);
            throw new IOException(e);
        }
        return tagList;
    }

    @Override
    public void indexCommit(String id) throws IOException {
        if (differentsCommits.isEmpty()) {
            logger.error("Differences is not calculated. List commits first!");
            throw new IOException("Differences is not calculated. List commits first!");
        }
        ObjectId objectId = repository.resolve(id);
        addCommitItem(repository.parseCommit(objectId));
    }

    @Override
    public void indexAllCommit() throws Exception {
        synchronized (commitRest) {
            if (!commitRest.isIndexExists()) {
                commitRest.createIndex();
            }
        }
        try {
            differentsCommits.clear();
            for (RevCommit commit : git.log().all().call()) {
                if (commit.getParents().length > 0) {
                    differentsCommits.put(toCommit(commit), countDifferences(commit, commit.getParent(0)));
                } else {
                    Different<Commit> different = new Different<>();
                    different.setFirst(toCommit(commit));
                    differentsCommits.put(toCommit(commit), different);
                }
                addCommitItem(commit);
            }
        } catch (GitAPIException e) {
            logger.error(e.getMessage(), e);
            throw new Exception(e);
        }
    }

    @Override
    public void indexTag(String id) throws IOException {
        if (tagMap.isEmpty()) {
            logger.error("Differences is not calculated. List tags first!");
            throw new IOException("Differences is not calculated. List tags first!");
        }
        tagRest.addTag(tagMap.get(id));
    }

    @Override
    public void indexAllTag() throws Exception {
        synchronized (tagRest) {
            if (!tagRest.isIndexExists()) {
                tagRest.createIndex();
            }
        }
        try {
            List<Ref> call = git.tagList().call();
            for (Ref ref : call) {
                LogCommand log = git.log();
                Ref peeledRef = repository.peel(ref);
                if (peeledRef.getPeeledObjectId() != null) {
                    log.add(peeledRef.getPeeledObjectId());
                } else {
                    log.add(ref.getObjectId());
                }

                Tag elasticTag = calculateDifferents(ref, log);
                tagRest.addTag(elasticTag);
            }
        } catch (GitAPIException e) {
            logger.error(e.getMessage(), e);
            throw new Exception(e);
        }
    }

    @Override
    public void close() {
        try {
            commitRest.close();
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
        try {
            tagRest.close();
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
        if (repository != null) {
            repository.close();
        }
    }

    private Tag calculateDifferents(Ref ref, LogCommand log) throws GitAPIException {
        Tag elasticTag = new Tag();
        elasticTag.setId(ref.getObjectId().getName());
        elasticTag.setName(ref.getName());
        List<String> commits = new ArrayList<>();
        for (RevCommit commit : log.call()) {
            commits.add(toCommit(commit).toString());
            if (commit.getParents().length > 0) {
                Different<Commit> different = countDifferences(commit, commit.getParent(0));
                elasticTag.setAddedLines(elasticTag.getAddedLines() + different.getAddedLines());
                elasticTag.setChangedFiles(elasticTag.getChangedFiles() + different.getFilesChanged());
                elasticTag.setRemovedLines(elasticTag.getRemovedLines() + different.getRemoveLines());
            }
        }
        elasticTag.setCommits(commits);
        return elasticTag;
    }

    private void cloneRepository(String git, SshSessionFactory sshSessionFactory) throws IOException, GitAPIException {
        File localPath;
        try {
            CloneCommand cloneCommand = Git.cloneRepository();
            cloneCommand.setURI(git);
            localPath = File.createTempFile("Git-" + new Date().getTime(), "");
            if (!localPath.delete()) {
                throw new IOException("Could not delete temporary file " + localPath);
            }
            cloneCommand.setDirectory(localPath);
            cloneCommand.setTransportConfigCallback(transport -> {
                SshTransport sshTransport = (SshTransport) transport;
                sshTransport.setSshSessionFactory(sshSessionFactory);
            });
            this.git = cloneCommand.call();
            repository = this.git.getRepository();
        } catch (IOException | GitAPIException e) {
            logger.error(e.getMessage(), e);
            throw new IOException(e);
        }
    }

    private Different<Commit> countDifferences(RevCommit commit, RevCommit parent) {
        Different<Commit> different = new Different<>();
        DiffFormatter df = new DiffFormatter(DisabledOutputStream.INSTANCE);
        df.setRepository(repository);
        df.setDiffComparator(RawTextComparator.DEFAULT);
        df.setDetectRenames(true);
        List<DiffEntry> diffs;
        try {
            diffs = df.scan(commit, parent);
            different.setFirst(toCommit(commit));
            different.setLast(toCommit(parent));
            different.setFilesChanged(diffs.size());
            for (DiffEntry diff : diffs) {
                for (Edit edit : df.toFileHeader(diff).toEditList()) {
                    different.setAddedLines(
                            different.getAddedLines() + (edit.getEndB() - edit.getBeginB()));
                    different.setRemoveLines(
                            different.getRemoveLines() + (edit.getEndA() - edit.getBeginA()));
                }
            }
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
        return different;
    }

    private void countDifferences(RevCommit commit, RevCommit parent, Commit elasticCommit) {
        DiffFormatter df = new DiffFormatter(DisabledOutputStream.INSTANCE);
        df.setRepository(repository);
        df.setDiffComparator(RawTextComparator.DEFAULT);
        df.setDetectRenames(true);
        List<DiffEntry> diffs;
        try {
            diffs = df.scan(commit, parent);
            elasticCommit.setChangedFiles(diffs.size());
            for (DiffEntry diff : diffs) {
                for (Edit edit : df.toFileHeader(diff).toEditList()) {
                    elasticCommit.setAddedLines(
                            elasticCommit.getAddedLines() + (edit.getEndB() - edit.getBeginB()));
                    elasticCommit.setRemovedLines(
                            elasticCommit.getRemovedLines() + (edit.getEndA() - edit.getBeginA()));
                }
            }
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
    }

    private void addCommitItem(RevCommit commit) throws IOException {
        Commit elasticCommit = toCommit(commit);
        Different<Commit> diff = differentsCommits.get(elasticCommit);
        elasticCommit.setChangedFiles(diff.getFilesChanged());
        elasticCommit.setRemovedLines(diff.getRemoveLines());
        elasticCommit.setAddedLines(diff.getAddedLines());
        commitRest.addCommit(elasticCommit);
    }

    private Commit toCommit(RevCommit revCommit) {
        Commit commit = new Commit();
        commit.setId(revCommit.getName());
        commit.setDate(revCommit.getCommitterIdent().getWhen());
        commit.setAuthor(new String(revCommit.getAuthorIdent().getName().getBytes(revCommit.getEncoding()))
                + "<" + revCommit.getAuthorIdent().getEmailAddress() + ">");
        commit.setMessage(new String(revCommit.getFullMessage().getBytes(revCommit.getEncoding())));
        List<String> parents = new ArrayList<>();
        for (RevCommit parentCommit : revCommit.getParents()) {
            parents.add(parentCommit.getName());
        }
        commit.setParents(parents);
        return commit;
    }
}
