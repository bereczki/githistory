package com.thotsoft.githistory.server.services;

import com.thotsoft.githistory.api.model.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;

public class ElasticTagItemIterator implements Iterator {
    private static final Logger logger = LoggerFactory.getLogger(ElasticTagItemIterator.class);

    private final TagRest gitRest;
    private final String scrollId;
    private List<Tag> list;
    private int current = 0;

    ElasticTagItemIterator(TagRestImpl gitRest, List<Tag> list, String s) {
        this.gitRest = gitRest;
        this.list = list;
        this.scrollId = s;
    }

    @Override
    public boolean hasNext() {
        if (current < list.size()) {
            return true;
        } else {
            current = 0;
            return nextScroll();
        }
    }

    @Override
    public Tag next() {
        if (!hasNext()) {
            return null;
        }
        return list.get(current++);
    }

    private boolean nextScroll() {
        try (InputStream stream = gitRest.scrolling(20, scrollId)) {
            list = new GitUtility().readTag(stream);
            return !list.isEmpty();
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
        return false;
    }
}
