package com.thotsoft.githistory.server.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.thotsoft.githistory.api.model.Commit;
import com.thotsoft.githistory.api.model.Tag;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

class GitUtility {
    private final SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss,SSS",
            Locale.ENGLISH);
    private final Gson gson = new GsonBuilder().setDateFormat(dateformat.toPattern()).create();


    /**
     * @param in InputStream that contains json
     * @return List of CommitItems
     * @throws IOException Exception during reading
     */
    List<Commit> readCommit(final InputStream in) throws IOException {
        final List<Commit> list = new ArrayList<>();
        final JsonReader reader = new JsonReader(new InputStreamReader(in));
        reader.beginObject();
        while (!"hits".equals(reader.nextName())) {
            reader.skipValue();
        }
        reader.beginObject();
        while (!"hits".equals(reader.nextName())) {
            reader.skipValue();
        }
        reader.beginArray();
        while (reader.hasNext()) {
            reader.beginObject();
            list.add(readCommitObject(reader));
            reader.endObject();
        }
        return list;
    }

    /**
     * @param in       InputStream that contains json
     * @param scrollId Outparameter for scrollid
     * @return List of CommitItems
     * @throws IOException Exception during reading
     */
    List<Commit> readCommit(final InputStream in, final StringBuilder scrollId)
            throws IOException {
        final List<Commit> list = new ArrayList<>();
        final JsonReader reader = new JsonReader(new InputStreamReader(in));
        reader.beginObject();
        if (scrollId != null) {
            if ("_scroll_id".equals(reader.nextName())) {
                scrollId.append(reader.nextString());
            }
        }
        while (!"hits".equals(reader.nextName())) {
            reader.skipValue();
        }
        reader.beginObject();
        while (!"hits".equals(reader.nextName())) {
            reader.skipValue();
        }
        reader.beginArray();
        while (reader.hasNext()) {
            reader.beginObject();
            list.add(readCommitObject(reader));
            reader.endObject();
        }
        return list;
    }

    /**
     * @param reader JsonReader
     * @return Commit entity
     * @throws IOException Exception during read
     */
    private Commit readCommitObject(final JsonReader reader) throws IOException {
        //Source
        while (!"_source".equals(reader.nextName())) {
            reader.skipValue();
        }
        final Commit item = gson.fromJson(reader, new TypeToken<Commit>() {
        }.getType());
        reader.nextName();
        reader.skipValue();
        return item;
    }

    /**
     * @param in InputStream
     * @return a Commit
     * @throws IOException if something happened during reading
     */
    Commit readCommitJson(final InputStream in) throws IOException {
        final JsonReader reader = new JsonReader(new InputStreamReader(in));
        reader.beginObject();
        return readCommitObject(reader);
    }

    /**
     * GET serverlog/log/_search?scroll=10m
     */
    static String getSearchJSON(final int size) {
        return "{\n" +
                "  \"sort\": [\n" +
                "    {\n" +
                "      \"date\": {\n" +
                "        \"order\": \"asc\"\n" +
                "      }\n" +
                "    }\n" +
                "  ], \n" +
                "  \"query\": {\n" +
                "    \"match_all\": {}\n" +
                "  }\n" +
                "  , \"size\": " + size + "\n" +
                "}";
    }

    Tag readTagJson(InputStream inputStream) throws IOException {
        final JsonReader reader = new JsonReader(new InputStreamReader(inputStream));
        reader.beginObject();
        return readTagObject(reader);
    }

    /**
     * @param reader JsonReader
     * @return TagItem entity
     * @throws IOException Exception during read
     */
    private Tag readTagObject(final JsonReader reader) throws IOException {
        //Source
        while (!"_source".equals(reader.nextName())) {
            reader.skipValue();
        }
        final Tag item = gson.fromJson(reader, new TypeToken<Tag>() {
        }.getType());
        reader.nextName();
        reader.skipValue();
        return item;
    }

    /**
     * @param in InputStream that contains json
     * @return List of Tags
     * @throws IOException Exception during reading
     */
    List<Tag> readTag(final InputStream in) throws IOException {
        final List<Tag> list = new ArrayList<>();
        final JsonReader reader = new JsonReader(new InputStreamReader(in));
        reader.beginObject();
        while (!"hits".equals(reader.nextName())) {
            reader.skipValue();
        }
        reader.beginObject();
        while (!"hits".equals(reader.nextName())) {
            reader.skipValue();
        }
        reader.beginArray();
        while (reader.hasNext()) {
            reader.beginObject();
            list.add(readTagObject(reader));
            reader.endObject();
        }
        return list;
    }

    /**
     * @param in       InputStream that contains json
     * @param scrollId Outparameter for scrollid
     * @return List of Tags
     * @throws IOException Exception during reading
     */
    List<Tag> readTag(final InputStream in, final StringBuilder scrollId)
            throws IOException {
        final List<Tag> list = new ArrayList<>();
        final JsonReader reader = new JsonReader(new InputStreamReader(in));
        reader.beginObject();
        if (scrollId != null) {
            if ("_scroll_id".equals(reader.nextName())) {
                scrollId.append(reader.nextString());
            }
        }
        while (!"hits".equals(reader.nextName())) {
            reader.skipValue();
        }
        reader.beginObject();
        while (!"hits".equals(reader.nextName())) {
            reader.skipValue();
        }
        reader.beginArray();
        while (reader.hasNext()) {
            reader.beginObject();
            list.add(readTagObject(reader));
            reader.endObject();
        }
        return list;
    }
}
