package com.thotsoft.githistory.server.services;


import com.thotsoft.githistory.api.model.Tag;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

public interface TagRest {
    boolean isIndexExists() throws IOException;

    void createIndex() throws IOException;

    void removeIndex() throws IOException;

    Tag getTag(String id) throws IOException;

    Iterator<Tag> getTags(int documentPerScroll, StringBuilder generatedScrollId) throws IOException;

    InputStream scrolling(int scroll, String scrollId) throws IOException;

    boolean addTag(Tag tag) throws IOException;

    boolean removeTag(String id) throws IOException;

    void close() throws IOException;
}
