package com.thotsoft.githistoryclient;

import com.thotsoft.githistory.api.GitService;
import org.junit.Test;

import javax.naming.NamingException;
import java.io.IOException;
import java.util.List;

public class ConnectServerTest {
    @Test
    public void connectLocalRepoTestForCommit() throws NamingException, IOException {
        try (ConnectServer repo =
                     new ConnectServer(new GitHistoryClient("localhost", 8080))) {
            GitService service = repo.connect();
            service.gitLocal("C:\\work\\serverlogindexer\\.git");
            List<String> commitIds = service.listCommits();
            for (String id : commitIds) {
                service.indexCommit(id);
            }
        }
    }

    @Test
    public void connectRemoteRepoTestForCommit() throws Exception {
        try (ConnectServer repo = new ConnectServer(new GitHistoryClient("localhost", 8080))) {
            GitService service = repo.connect();
            service.gitRemote("gitlab@git.thot-soft.com:thot-soft-inner/shopping-list.git",
                    "-----BEGIN RSA PRIVATE KEY-----\n" +
                            "Proc-Type: 4,ENCRYPTED\n" +
                            "DEK-Info: AES-128-CBC,22C57F0602AB414C1592E5ABC0138557\n" +
                            "\n" +
                            "WZj4OQALzmF0ORossMXmMoFKiyOpYFy1YFW0ekSNwb6B9ZFY26O9CFQJo/D7MRMp\n" +
                            "u+aHisv3gQevfya2svW6aNuuIEbPGuNoJ58PZiS6r8H3QId5EW1td08H6wa1JQqx\n" +
                            "23YZKG15kMpzzDf51+EfQQSmaGXSuNflhhrzAnY7gKnS3xKI0hazvWr6FHnONC2e\n" +
                            "SgFHePK97uM8MoQ545f9IFYwv2KQQDDQqmGkE3sGWwD9TZMD23fy9FZHW0KxU+ry\n" +
                            "45fJLzuqIX59JexvsgteVSJmPWDAxbE6ucwEIYzwQgXzGMNzn/5QNmlgqSuGHRa4\n" +
                            "Z4j5NxBQCl5YHOqFa7Mq0OUQcvVqayRuQPk/nUTSX2aA0arQ1E0+NFcnuYPxw/Rx\n" +
                            "/KSlXv1oUeSWxv9XtC8Dkv8lXxGrspF65A36LW4AwL7OmY3aX1JiypHQfJvL9VBs\n" +
                            "q+lUtF5uCzRadoQnt5TrAGcLsztFPMA4s2lLMPpKgS6Ph3DVN0ulmi/VihJtuiby\n" +
                            "uJEuqSDY23FW2O1DQmiX2F1Hrlc6voZW97N/QR7wKWgJMcrOl0d/NU4Tt4cOszpq\n" +
                            "j/2/qPw/8AH8q+DhAwdHKP0adSm4TlTuUDbF0JAPlRNNL8KiICMmiDdNs2mphdHn\n" +
                            "gvNSRRHcqGF3VGzyhcgBdT+rlThKiP0tlfPSEm6gTnS4z/3ikJAQyQIEVh0JAVbd\n" +
                            "ay2rOEVC72aK0A86WMf5ilbSAyVjgJDZO8/DSIHQYQVVeypTq3hc/ppXlw0pRM1A\n" +
                            "Q5KRI4iSdeMXPEdlLVjMy2KtEwjSzSqohqDl4FWyX5MGlqfwsJ5sbY+VUxKzinhq\n" +
                            "27WdcEOtIZzQHvaFd2xBtzVEBebt9pOcofWwtU1uIGxsR0jSzNzHAjcxknBa05Wf\n" +
                            "SOo1CXfPXEJeD0Y2B2lAikFYgY8Q9yc88tMFJTXcDEfFlQXUCdTEysm6C2+Ak+fD\n" +
                            "nIcOrK8Nl9qzD2ylqVUlrx9UB1m1pDoJeZ38ql27mG4wg17bdsTu+L7wnNhotQdZ\n" +
                            "EOc11HvFcGktW1mI2xx22vi2U8hqrZLLAr4+n3jHSvzhqGExoJzyVU/mrOX0H0IA\n" +
                            "jeJf5qwQYV0rcZbXEkDEdYkYnXa9CV1E9whyKXMZCIj4KNgnEBadMAk74Zm/To3H\n" +
                            "H9KD0QhwUjdN6EBP/5jBv5UVGCYwPfowiBMGgzbuDav/IeIb+yQB+5wKSbC24R2L\n" +
                            "3FbCDTtBpHAGZpXazlJ+xJT2NVbPfgedn850y9ywcKblHsSJVuFH85bMzIy8IjBx\n" +
                            "+tw/TmB7gFFLvYDc17a+P5MYlMuCAmtZ67no2cJEvz0FPrmZT+1OkfJNoCydHz4o\n" +
                            "EXJEijXbJHOxgEVOExJjdp4p2V9UkY9Nmjwd+0Yvv+rAwx1uM7iMV7jp7DMCX5lj\n" +
                            "SBj3ukYB+fU0w6cVkuP0iAe2TeQJeGDBV5wkJBWYkWUasPc4Br29mI253Zn9Rg8H\n" +
                            "EOYE+uWKTzLnGYiOFzoIvHMCvVpMMeC6JYo7TgQcPoqgLRDC3pFE+6iEW4YsSTbg\n" +
                            "DuKAaAjiWHRWW6r6gzo+0Rm4RKs4w7MTNTb6nZOVxicXROJa1Wv2B/f9GMHBobvT\n" +
                            "rEBlmbmllFPg0uQDgQS4uzofOSjm6salCk9fzv3jGLt5SQwyA7LJCkuo/fERnWKU\n" +
                            "+QIPW1q3SVXBQfjQMlBJRXwhN1FLrhYeanHbOxW9qF6sk7rlEd5xzjF66BLg1mTs\n" +
                            "CSDDkbvfX49iDuAb/1kLUfbWvb3FqWx3pK5TUorFQN7ZZnx7CdWevoMMkCvLsvs1\n" +
                            "xH/OhNwWay0N0fAzRbq+hNMAAJ8vSh7cOLQQJyjONJjJeQM0Mvpw3dvI851l+/0V\n" +
                            "Hfc0nY+LrTJocmZQbbZJT90WbI6TyAG5XvOxc2jmNVhjSiGcV4aTilR3L+BUgIE5\n" +
                            "gfP8scMT+Q89rhvfvaKbxrnFs9jKg4hEjSMDrL0FWuRKedliFOKJ94Xvw6ylbW1N\n" +
                            "pD0Izz+kh1rYdEvNEuFZuPDEpflQ79ipixcz0hDAmHYpBpM+f0c7ZxW/O9Gld/PU\n" +
                            "/+OMcFLjjhVx1rcDGPYgKfZSrxjamwG1dSUahj/6d8Ke/BDIOaXYEo4yj/Whkb57\n" +
                            "nn+SutZ752qISK+L7V1dfBmHj003VMExiEGXfkFdZUVbSveG54pYLvW5kdGOd5+A\n" +
                            "utNWDQQwrEOM8PPij0FAP+w0yQ2Oe//NuN9hDCsg+6OoKi31NdG4iCKs2XSwFc1k\n" +
                            "IS7W3eL0eMjfe/We4WVEdennlq8rfRvwQBIPNZaNFCztcoxmRP8N3isln/GT5wu1\n" +
                            "J9tLD/FJOH2QgQYYrm/9J15BxLabrQv170YRoW12Ya4BLy7SX4IoGPBN9lYrC5oE\n" +
                            "C93kfvQPycxmJ61UiuxOE/5GEADrKdUuFxXOR0Hs7sStLxx2pd4ruYGTYOb9huiM\n" +
                            "kI78dKGq7yGYpHmoHT+K378daVkehwYARo5yhzFdjTPYWizaNZas/ABEuiinE4uF\n" +
                            "QFermaMs2t1IBeLj1Us8gP5eF1m1BNHDw0s4Dxs/v4tun7e9AeZ5frDjrLxDCO4J\n" +
                            "QzIAa0tA8rJ+VNtA+5UA0tjaOahLpTFaCdJhfDOjYK99MSfx+jbH1ysGKpLuQqby\n" +
                            "X7vHtcSUU7SZB+9L+VwxivmC5qwJNkWfi47xFZmQL5vzucTzNXaKAnRtWAkVKJoq\n" +
                            "m1eHqF+/jzJe4sxIN5a4T90SznHifI/NgT1C9u2Lm10MC+pobmteu6z1Tmb5Yn+5\n" +
                            "5glTuLXIlmXRjDFPd7tXv6uCknLPreaCDPxWeLOxI2zXFuLC6ZTr0Wr/cB8gelo+\n" +
                            "AKzWizGdZzoYVX8VvoXXbvrMuF+PztdR4O02KhcFK83e9JH5CVWtlZ2uy0AOF0KS\n" +
                            "khv90TQYTOGOk1hnFYDcZhC3c1DaOoO2lbyYrPc8JtEKkMX09aCo4mKBxrUTr7yf\n" +
                            "7ReqlyG9v8+vlL/m//jKGTY0bxnrv/7cm1xwbQ4Wc1xOw7XN5bKTBizA7orMd5Ox\n" +
                            "UBRcFmVsuMxW10iRSb7+ni8RsAY2xJdluFJT3/pYM4QZ0EwLzKxJCLyyx/HhUcZa\n" +
                            "bEbv9FlL3yyE0z43Z8rxIOBX8nYQuvN3kZlEANCe/vtvm6bcgHSzhwPmUeihrmov\n" +
                            "-----END RSA PRIVATE KEY-----\n",
                    "b19t94RMW");
            for (String id : service.listCommits()) {
                service.indexCommit(id);
            }
            service.close();
        }
    }

    @Test
    public void connectLocalRepoTestForTag() throws Exception {
        try (ConnectServer repo = new ConnectServer(new GitHistoryClient("localhost", 8080))) {
            GitService service = repo.connect();
            service.gitLocal("C:\\work\\serverlogindexer\\.git");
            List<String> tagIds = service.listTags();
            for (String id : tagIds) {
                service.indexTag(id);
            }

        }
    }
}