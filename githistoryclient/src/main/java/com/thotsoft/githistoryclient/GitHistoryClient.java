package com.thotsoft.githistoryclient;

public class GitHistoryClient {
    private String host;
    private int port;
    private String type;

    public GitHistoryClient(String host, int port) {
        this.host = host;
        this.port = port;
        this.type = type;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
