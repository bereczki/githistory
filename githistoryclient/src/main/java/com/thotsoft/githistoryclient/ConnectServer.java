package com.thotsoft.githistoryclient;

import com.thotsoft.githistory.api.GitService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Properties;

public class ConnectServer implements AutoCloseable {
    private static final Logger logger = LoggerFactory.getLogger(ConnectServer.class);
    private GitHistoryClient client;
    private InitialContext remotingContext;

    public ConnectServer() {
        client = new GitHistoryClient("localhost", 8080);
    }

    public ConnectServer(GitHistoryClient client) {
        this.client = client;
    }

    public GitService connect() throws NamingException {
        String host = client.getHost();
        String port = Integer.toString(client.getPort());
        remotingContext = createRemoteEjbContext(host, port);

        // Syntax: ejb:${appName}/${moduleName}/${beanName}!${remoteView}
        // appName = name of EAR deployment (or empty for single EJB/WAR
        // deployments)
        // moduleName = name of EJB/WAR deployment
        // beanName = name of the EJB (Simple name of EJB class)
        // remoteView = fully qualified remote interface class
        String ejbUrl = "githistory-war-1.0/GitServiceImpl!com.thotsoft.githistory.api.GitService";
        return (GitService) remotingContext.lookup(ejbUrl);
    }

    /**
     * Create Remote EJB Context.
     *
     * @param host host to connect to (e.g. "127.0.0.1")
     * @param port port to connect to (wildfly HTTP port, e.g. 8080)
     * @return remote EJB context
     * @throws NamingException if creating the context fails
     */
    private InitialContext createRemoteEjbContext(String host, String port) throws NamingException {
        Properties jndiProps = new Properties();
        jndiProps.put(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.naming.remote.client.InitialContextFactory");
        jndiProps.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
        jndiProps.put("jboss.naming.client.ejb.context", true);

        jndiProps.put(Context.PROVIDER_URL, "http-remoting://" + host + ":" + port);
        return new InitialContext(jndiProps);
    }

    public void close() {
        if (remotingContext != null) {
            try {
                remotingContext.close();
            } catch (NamingException e) {
                logger.error(e.getMessage(), e);
            }
        }
    }
}
