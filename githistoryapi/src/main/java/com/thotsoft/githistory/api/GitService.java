package com.thotsoft.githistory.api;

import java.io.IOException;
import java.util.Date;
import java.util.List;

public interface GitService {

    void gitRemote(String git, String SSHPrivateKey) throws IOException;

    void gitRemote(String git, String SSHPrivateKey, String password) throws IOException;

    void gitLocal(String pathToGit) throws IOException;

    List<String> listCommits() throws IOException;

    List<String> listCommits(Date fromDate, Date toDate) throws IOException;

    List<String> listTags() throws IOException;

    void indexCommit(String id) throws IOException;

    void indexAllCommit() throws Exception;

    void indexTag(String id) throws IOException;

    void indexAllTag() throws Exception;

    void close();
}
