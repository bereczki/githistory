package com.thotsoft.githistory.api.model;

import java.io.Serializable;

public class Different<T> implements Serializable {
    private T first;
    private T last;
    private int addedLines;
    private int removeLines;
    private int filesChanged;

    public T getFirst() {
        return first;
    }

    public void setFirst(T first) {
        this.first = first;
    }

    public T getLast() {
        return last;
    }

    public void setLast(T last) {
        this.last = last;
    }

    public int getAddedLines() {
        return addedLines;
    }

    public void setAddedLines(int difference) {
        this.addedLines = difference;
    }

    public int getRemoveLines() {
        return removeLines;
    }

    public void setRemoveLines(int removeLines) {
        this.removeLines = removeLines;
    }

    public int getFilesChanged() {
        return filesChanged;
    }

    public void setFilesChanged(int filesChanged) {
        this.filesChanged = filesChanged;
    }


    @Override
    public String toString() {
        return "Different{" +
                "first=" + first +
                ", last=" + last +
                ", addedLines=" + addedLines +
                ", removeLines=" + removeLines +
                ", filesChanged=" + filesChanged +
                '}';
    }
}
